import React, { useState } from 'react'
import { connect } from 'react-redux'

import { addCurrentEvent } from './../redux/actionCreators'

let events = [
    {
        date: '17-04-2021',
        name: 'Boots and hearts Music Festival, Toronto',
        email: 'tolulopeobaakosile@gmail.com',
        price: 400,
        emailPrice: 350,
        discount: 12,
        ticket: 'Tickletene',
        trackingNumber: '1000f',
        status: 'Pending',
    },
    {
        date: '17-04-2021',
        name: 'Boots and hearts Music Festival, Toronto',
        email: 'tolulopeobaakosile@gmail.com',
        price: 400,
        emailPrice: 350,
        discount: 12,
        ticket: 'Tickletene',
        trackingNumber: '1000f',
        status: 'Pending',
    },
    {
        date: '17-04-2021',
        name: 'Boots and hearts Music Festival, Toronto',
        email: 'tolulopeobaakosile@gmail.com',
        price: 400,
        emailPrice: 350,
        discount: 12,
        ticket: 'Tickletene',
        trackingNumber: '1000f',
        status: 'Pending',
    },
    {
        date: '17-04-2021',
        name: 'Boots and hearts Music Festival, Toronto',
        email: 'tolulopeobaakosile@gmail.com',
        price: 400,
        emailPrice: 350,
        discount: 12,
        ticket: 'Tickletene',
        trackingNumber: '1000f',
        status: 'Pending',
    },
    {
        date: '17-04-2021',
        name: 'Boots and hearts Music Festival, Toronto',
        email: 'tolulopeobaakosile@gmail.com',
        price: 400,
        emailPrice: 350,
        discount: 12,
        ticket: 'Tickletene',
        trackingNumber: '1000f',
        status: 'Pending',
    },
    {
        date: '17-04-2021',
        name: 'Boots and hearts Music Festival, Toronto',
        email: 'tolulopeobaakosile@gmail.com',
        price: 400,
        emailPrice: 350,
        discount: 12,
        ticket: 'Tickletene',
        trackingNumber: '1000f',
        status: 'Pending',
    },
    {
        date: '17-04-2021',
        name: 'Boots and hearts Music Festival, Toronto',
        email: 'tolulopeobaakosile@gmail.com',
        price: 400,
        emailPrice: 350,
        discount: 12,
        ticket: 'Tickletene',
        trackingNumber: '1000f',
        status: 'Pending',
    },
    {
        date: '17-04-2021',
        name: 'Boots and hearts Music Festival, Toronto',
        email: 'tolulopeobaakosile@gmail.com',
        price: 400,
        emailPrice: 350,
        discount: 12,
        ticket: 'Tickletene',
        trackingNumber: '1000f',
        status: 'Pending',
    },
    {
        date: '17-04-2021',
        name: 'Boots and hearts Music Festival, Toronto',
        email: 'tolulopeobaakosile@gmail.com',
        price: 400,
        emailPrice: 350,
        discount: 12,
        ticket: 'Tickletene',
        trackingNumber: '1000f',
        status: 'Pending',
    },
    {
        date: '17-04-2021',
        name: 'Boots and hearts Music Festival, Toronto',
        email: 'tolulopeobaakosile@gmail.com',
        price: 400,
        emailPrice: 350,
        discount: 12,
        ticket: 'Tickletene',
        trackingNumber: '1000f',
        status: 'Pending',
    },
    {
        date: '17-04-2021',
        name: 'Boots and hearts Music Festival, Toronto',
        email: 'tolulopeobaakosile@gmail.com',
        price: 400,
        emailPrice: 350,
        discount: 12,
        ticket: 'Tickletene',
        trackingNumber: '1000f',
        status: 'Pending',
    },
]


const TransactionList = ({addCurrentEvent}) => {

    const [eventSelected, setEventSelected] = useState(events[0])

    const selectEvent = (key) => {
        setEventSelected(events[key])
    }
    
    return (
        <div className="transaction-list-container">
            <div className="transaction-list">
                <h2 className="transaction-list__title">Transaction list</h2>
                <table className="events-list__table">
                    <thead className="events-list__table-head">
                        <th className="events-list__table-head-row">Date</th>
                        <th className="events-list__table-head-row">Event</th>
                        <th className="events-list__table-head-row">Email</th>
                        <th className="events-list__table-head-row">Price</th>
                        <th className="events-list__table-head-row">Mail price</th>
                        <th className="events-list__table-head-row">Discount</th>
                        <th className="events-list__table-head-row">Ticket</th>
                        <th className="events-list__table-head-row">Traking number</th>
                        <th className="events-list__table-head-row">Status</th>
                    </thead>
                    <tbody className="events-list__table-body">
                        {
                            events.map((event, key) => (
                                <tr className="events-list__table-body-row" onClick={() => {selectEvent(key)}}>
                                    <td className="events-list__table-body-data"><span className="events-list__table-body-date">{event.date}</span></td>
                                    <td className="events-list__table-body-data"><span className="events-list__table-body-name">{event.name}</span></td>
                                    <td className="events-list__table-body-data"><span className="events-list__table-body-owner">{event.email}</span></td>
                                    <td className="events-list__table-body-data"><span className="events-list__table-body-name">{`$${event.price}`}</span></td>
                                    <td className="events-list__table-body-data"><span className="events-list__table-body-date">{`$${event.emailPrice}`}</span></td>
                                    <td className="events-list__table-body-data"><span className="events-list__table-body-owner">{`${event.discount}%`}</span></td>
                                    <td className="events-list__table-body-data"><span className="events-list__table-body-owner">{event.ticket}</span></td>
                                    <td className="events-list__table-body-data"><span className="events-list__table-body-owner">{event.trackingNumber}</span></td>
                                    <td className="events-list__table-body-data"><span className="events-list__table-body-owner">{event.status}</span></td>
                                </tr>
                            ))
                        }
                    </tbody>
                </table>
            </div>
        </div>
    )
}

const mapDispatchToProps = (dispatch) => {
    return {
        addCurrentEvent: (event) => {dispatch(addCurrentEvent(event))}
    }
}

export default connect(null, mapDispatchToProps)(TransactionList)