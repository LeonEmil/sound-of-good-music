import React from 'react'

const images = [
    {
        src: 'https://elonce-media2.elonce.com.ar/fotos-nuevo/2021/02/04/o_1612435907.jpg',
        alt: 'Historic palace'
    },
    {
        src: 'https://elonce-media2.elonce.com.ar/fotos-nuevo/2021/02/04/o_1612435907.jpg',
        alt: 'Historic palace'
    },
    {
        src: 'https://elonce-media2.elonce.com.ar/fotos-nuevo/2021/02/04/o_1612435907.jpg',
        alt: 'Historic palace'
    },
    {
        src: 'https://elonce-media2.elonce.com.ar/fotos-nuevo/2021/02/04/o_1612435907.jpg',
        alt: 'Historic palace'
    },
    {
        src: 'https://elonce-media2.elonce.com.ar/fotos-nuevo/2021/02/04/o_1612435907.jpg',
        alt: 'Historic palace'
    },
    {
        src: 'https://elonce-media2.elonce.com.ar/fotos-nuevo/2021/02/04/o_1612435907.jpg',
        alt: 'Historic palace'
    },
]

const VenueInformation = () => {
    return (
        <div className="venue-information-container">
            <h2 className="venue-information-container__title">Venue information</h2>
            <div className="venue-information">
                <div className="venue-information-image-container">
                    <img src='https://elonce-media2.elonce.com.ar/fotos-nuevo/2021/02/04/o_1612435907.jpg' alt='Historic palace' className="venue-information-image" />
                </div>
                <div className="venue-information-wrapper">
                    <h1 className="venue-information__title">Bill Reid Gllery of Northwest Coast Art</h1>
                    <p className="venue-information__venue-title">Venue Owner: </p>
                    <p className="venue-information__venue">Judge Smith</p>
                    <p className="venue-information__capacity-title">Capacity: </p>
                    <p className="venue-information__capacity">120 seats</p>
                    <p className="venue-information__address-title">Address: </p>
                    <p className="venue-information__address">6390 Hornby St, Vancouver, BC V6C 2G3, Canada.</p>
                    <p className="venue-information__description-title">Description</p>
                    <p className="venue-information__description">
                        Ticket volume or total tickets tracks all tickets in your support queue over a period of time. A variation of this metric is total conversations, which counts all engagement with
                    </p>
                </div>
            </div>
            <div className="venue-gallery-container">
                <h2 className="venue-gallery-container__title">Gallery</h2>
                <button className="venue-gallery-container__button">Manage</button>
                <div className="venue-gallery">
                    {
                        images.map((image, key) => (
                            <div key={key} className="venue-gallery__image-container">
                                <img src={image.src} alt={image.alt} className="venue-gallery__image" />
                            </div>
                        ))
                    }
                </div>
            </div>
        </div>
    )
}

export default VenueInformation